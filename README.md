# DATA-HUB: a hub for high speed sensor array
![alt text](assets/animated.jpg)

## A SPI hub for sensor arrays with high speed, low latency, and large inter-sensor distance 
DATA-HUB allows to connect Up to 12 sensors simultaneously using  ethernet cables with up to 20 meters(60 feet) of distance between them. DATA-HUB is designed for the recording of fields of data in real time. Sensors can be hot swapped allowing for maximum flexibility. 


## How does it look?
DATA-HUB presents itself as a shield for Arduino compatible boards. Any Arduino will work but it is recommended to use large format ones like the mega or even better the Due. These are faster than traditional (AVR based) Arduino; in particular the Due has a second usb that can be used for very high speed data transfer to a computer.
![alt text](assets/sensor-hub-small.jpg)


## How to use DATA-HUB

Here are the prerequisite for DATA-HUB:
- Arduino IDE installed (www.arduino.cc)
- Java installed (www.java.com)
- OPTIONAL: processing IDE (www.processing.org)

Once you have these installed: upload the code for HUB-SPI hardware on the Arduino, then insert the shield onto the arduino. Now you can plug the sensors to the hub. Lastly, start the recorder/visualizer (it's a processing patch, sources are here) from your computer, you should see the sensors emitting data onscreen.
 

## Current limitations
- The sensors can only be powered at 3.3v or less (with a regulator below 3.3v). This should not be a major issue as most sensors comes in 1.8-3.6v range. 


## Improvements
hot swapping sensors on older arduinos is prone to errors due to the slow serial connection, refactoring that part would allow a wider compatibility with the ecosystem.


## Get involved!
If you find a bug or if you see have made a sensor and would like to use the hub for it get in touch or even better, send a pull request.


### List of components


- Arduino firmwares
    - SPI version (fast speed multi sensor)
    - I2C version (old and obsolete, limited to 2 sensors, and 1.3 meters in length)

- Data collection (computer)
    Processing patch (java based) for data acquisition. You can use the application directly by selecting the one for your system (Windows,Linux or mac) or you can use the Processing patch directly but you will need to get the Processing app from your OS at processing.org

## Licence
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
![alt text](assets/creativeCommons.png)
