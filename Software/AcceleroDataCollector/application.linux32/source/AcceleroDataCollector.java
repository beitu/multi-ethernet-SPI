import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.nio.ByteBuffer; 
import java.nio.ShortBuffer; 
import java.nio.ByteOrder; 
import java.util.Arrays; 
import processing.serial.*; 
import controlP5.*; 
import java.util.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class AcceleroDataCollector extends PApplet {









final int screenSize   = 1200 ;

// -----------------------------------------------------------------
//SPI-HUB related
int sensorNumber             = 0 ;
int sensorDataReceivedAtOnce = 0 ;  
int serialBufferSize         = 5 ;     

// -----------------------------------------------------------------
// SENSORS AUTO-DETECTION
static final int SENSOR_AUTO_DETECT_CYCLE = 50 ; // number of buffer read before autodetecting sensors                                      
static int sensorAutoDetectionTimeout = 0;
static String autoDetectionStatus = "disabled";


// -----------------------------------------------------------------
// SENSOR SETUP (this example is for acceleroators )
ArrayList <Accelerometer>  accelerometers = new ArrayList();
short accelerometerSensitivity = 0 ; // in G, can be 2,4,8,16 etc... 
int   SensorDataSpeed = 0 ;
  


// -----------------------------------------------------------------
//SERIAL RELARTED
Serial serialPort;
String latestUSBDevicePlugged;
static final int MAX_BUFFER_SIZE      = 2048 ;
static final int SENSOR_SETTINGS_SIZE = 6 ;

ByteBuffer sensorValuesBuffer = ByteBuffer.allocate(MAX_BUFFER_SIZE);
ByteBuffer settingsBuffer     = ByteBuffer.allocate(SENSOR_SETTINGS_SIZE);
ShortBuffer sensorSettings ;
ShortBuffer sensorValues ;
int timeoutSync = 10 ;

static final int SERIAL_SPEED   = 5000000; //5000000; //460800; //230400 ;
static final int SERIAL_WAIT    = 1000 ;
static final int SERIAL_DELAY   = 5  ;  // delay between bi-directional communication
                                          // this needs to be at least 2ms since lower
                                          // values will mix serial buffers
//----------------
String sensorData = "";
PFont font;
PrintWriter output = null;
BufferedReader reader;
String line;
int timeToShow = 0;
Boolean programHasStarted = false;

// -----------------------------------------------------------------
//RECORDING SESSION RELATED
boolean recordingSessionStarted = false ;
static final int pastDataSize      = 3600 ;
static final int oldestDataPoint   = pastDataSize -1;
int recordingStartTime = 0;
int recordingStopTime  = 0 ; //millis();


// -----------------------------------------------------------------
// GRAPH RELATED SETUP
static final int startOfChartXAxis = 50 ;
static final int graphYOffset      = 100 ; //position of the graph (y)
static final int dataGraphResolution = 1; //every X data we plot an actual point on screen

int secondGraph       = 400 ; //position of the graph (y)
int YAxisScale = 3 ; // divides the height of the Y axis to adapt to screen size
int graphAxisDisplayOffset = 3 ;


// -----------------------------------------------------------------
// UI RELATED SETUP
ControlP5 cp5;
final String fileNameField = "Set File name first" ;
String textValue = "";
boolean dataFileHasAName = false;

    
     


// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
public void setup()
{
   
  
  
  latestUSBDevicePlugged =  Serial.list()[Serial.list().length -1 ] ;
  
  print("Trying to connect Serial port to : ",latestUSBDevicePlugged, " ..." );
  try{  serialPort = new Serial(this, Serial.list()[Serial.list().length -1 ], SERIAL_SPEED); }    catch (Exception e) { serialPort = null; }
  
  if (serialPort == null) {
    println("...no serial port");
    selectInput("Select a file to process:", "fileSelected");
    serialPort = null;
    exit();
  }
  else   {
    println("...serial initiated");
    //serialPort.buffer(serialBufferSize);
    
    PFont font = createFont("arial",18);
    cp5 = new ControlP5(this);
    cp5.addTextfield(fileNameField)
     .setPosition(20,20)
     .setSize(200,20)
     .setFont(font)
     .setFocus(true)
     .setColor(color(255,255,0))
    ;
    
    // Setting up the callback for Start button 
    CallbackListener hideStartButton = new CallbackListener() {
      public void controlEvent(CallbackEvent theEvent) {
        
        if (cp5.get(Textfield.class,fileNameField).getText() != null && (cp5.get(Textfield.class,fileNameField).getText().length() >0)){
          
          String dataFileName = cp5.get(Textfield.class,fileNameField).getText() ;
          InputStream testingFileExistence = createInput(dataFileName + ".txt");
          if (testingFileExistence == null){
            output = createWriter(dataFileName + ".txt");
            output.print(  "//---------------------------------" +
                         "\n// File name: " + dataFileName +
                         "\n// Time and Date : " + hour() + ":" + minute() + ":" + second() + " " + String.valueOf(day()) + "/" + String.valueOf(month()) + "/" + String.valueOf(year())+  
                         "\n// Accelerometer for this test:" + sensorNumber +
                         "\n// Data Collection speed is " + SensorDataSpeed + " Hz" +
                         "\n// Sensitivity: " + accelerometerSensitivity + "g" +
                         "\n//---------------------------------\n"
                          );
            theEvent.getController().hide();
            cp5.get(Textfield.class,fileNameField).hide() ;
            dataFileHasAName = true;
            recordingSessionStarted = true;
            recordingStartTime = millis();
            cp5.getController("Stop").show();
          }
        }
      }
    };
    
    // Setting up the callback for Start button 
    CallbackListener hideStopButton = new CallbackListener() {
      public void controlEvent(CallbackEvent theEvent) { exit(); }
    };
    
    cp5.addBang("Start")
     .setPosition(240,15)
     .setSize(50,30)
     .setId(1)
     .onClick(hideStartButton)
     .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
    ;     
    
    
    cp5.addBang("Stop")
     .setPosition(screenSize-100,15)
     .setSize(50,30)
     .setId(2)
     .onClick(hideStopButton)
     .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
    ;     
    
    cp5.getController("Stop").hide();

    textFont(font);

    //setup the serial transmission of data from sensors to computer
    delay(SERIAL_WAIT);
    
    //buf = ByteBuffer.allocate(serialBufferSize);
    //serialPort.buffer(2048);        
    programHasStarted = true; 
  }
}



// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
public void draw() {
  
     if (recordingSessionStarted) {
       background(0,0,0);
       text(cp5.get(Textfield.class,fileNameField).getText(), width/2, 40);
     }
     else {     
       if (sensorAutoDetectionTimeout == 0) checkHotSwapping();
       else sensorAutoDetectionTimeout--;
       background(0,0,64);
     }
  
     for (int i = 0 ; i < accelerometers.size(); i++) accelerometers.get(i).show(100+(i*graphYOffset));   
     surface.setTitle(str(PApplet.parseInt(frameRate)));
}

//--------------------------------------------------------
public void serialEvent (Serial serialPort) {
  
  if (!programHasStarted) { 
    print(serialPort.readString()); 
    return ; 
  }
      
  switch(autoDetectionStatus){
    case "disabled" :
    case "standby"  : sensorValuesBuffer =  ByteBuffer.allocate(serialBufferSize);
                      if (serialPort.available() != serialBufferSize) return ;
                      sensorValuesBuffer =  ByteBuffer.wrap(serialPort.readBytes());   
                      sensorValuesBuffer.order(ByteOrder.LITTLE_ENDIAN);
                      
                      sensorValues = sensorValuesBuffer.asShortBuffer();
                      
                      int currentValue = 0 ;
                      for(int i = 0 ; i< sensorDataReceivedAtOnce;i++){                        
                        for (int currentSensor = 0; currentSensor < sensorNumber ;currentSensor++) {
                          accelerometers.get(currentSensor).setX( sensorValues.get(currentValue++));   
                          accelerometers.get(currentSensor).setY( sensorValues.get(currentValue++)); 
                          accelerometers.get(currentSensor).setZ( sensorValues.get(currentValue++));   
                        }
                      }  
                      break;
  }
  
}





//-------------------------------------------------------------------------------------
public void checkHotSwapping(){
  //print(autoDetectionStatus,',');
  switch (autoDetectionStatus) {
    
           case "disabled"    :  serialPort.write('H'); //print('H');//HALTS the arduino
                                 autoDetectionStatus = "standby" ;
                                 break;
                                          
           case "standby"     :  if (serialPort.available() == 0) {
                                    serialPort.buffer(1);                                     
                                    autoDetectionStatus = "ready";
                                 }
                                 break;
                                          
           case "ready"      :   autoDetectionStatus = "receiving";
                                 serialPort.write('R'); //print("R");
                                 break;
                                 
          case "receiving"   : 
                                 if (serialPort.available() == SENSOR_SETTINGS_SIZE){
                                  //println("#",serialPort.available());
                                  settingsBuffer =  ByteBuffer.wrap(serialPort.readBytes());   
                                  settingsBuffer.order(ByteOrder.LITTLE_ENDIAN);
                                  sensorSettings = settingsBuffer.asShortBuffer();
                                  
                                  sensorDataReceivedAtOnce = sensorSettings.get(1);
                                  serialBufferSize = sensorSettings.get(2);
                                  
                                  //the following two lines' order matters
                                  int oldSensorNumber = sensorNumber;
                                  sensorNumber = sensorSettings.get(0);
                                                                   
                                  setupSensorsData(oldSensorNumber,sensorNumber); 
                                  serialPort.buffer(serialBufferSize); // set the system to only accept full dataframe
                                  autoDetectionStatus = "startingSensors";
                                  
                                 } 
                                 break;                                   

                                          
          case "startingSensors" : autoDetectionStatus = "disabled";
                                   sensorAutoDetectionTimeout = SENSOR_AUTO_DETECT_CYCLE ;
                                   serialPort.write('S');
                                   break;
                                          
          default: println("ERNO"); break;
         }
  
}

// ------------------------------------------------------------------------
// sets up the software based on the arduino settings and the sensors     
public void setupSensorsData(int oldSensorCount, int newSensorNumber){
    
  if(oldSensorCount == newSensorNumber) return;
    
  if (newSensorNumber > oldSensorCount  ){
    for (int i=oldSensorCount; i < newSensorNumber;i++ ) {
      accelerometers.add (new Accelerometer("A"+ i)); 
      accelerometers.get(i).setActive() ;
    }
  }
  else  for (int i=newSensorNumber; i < oldSensorCount;i++ )   accelerometers.remove(i);
  
}

//--------------------------------------------------------
public void mousePressed() {


  if ((mouseX) > startOfChartXAxis) return;
  //first accelero
  if (mouseY < secondGraph/2) {
    if (mouseY > 180) accelerometers.get(0).zHidden = !accelerometers.get(0).zHidden;
    else if (mouseY < 120) accelerometers.get(0).xHidden = !accelerometers.get(0).xHidden;
    else accelerometers.get(0).yHidden = !accelerometers.get(0).yHidden;
  }
  
  //second accelero
  else{
    if (mouseY > 480) accelerometers.get(1).zHidden = !accelerometers.get(1).zHidden;
    else if (mouseY < 450) accelerometers.get(1).xHidden = !accelerometers.get(1).xHidden;
    else accelerometers.get(1).yHidden = !accelerometers.get(1).yHidden;
  }
  
} 

// ---------------------------------------------------
// ---------------------------------------------------
class Accelerometer {
  short x,y,z ;
 int currentData ;

 short[] pastX, pastY, pastZ;
 String name;
 boolean isActive,xHidden, yHidden, zHidden;
 final boolean FILE_AS_SOURCE = true ;
 final boolean REALITY_AS_SOURCE = false ;
 boolean dataSource = REALITY_AS_SOURCE;


 //----------------------------
 Accelerometer(String newName){
   name = newName ;
   x = 0;
   y = 0;
   z = 0;
   isActive = false ;
   xHidden  = false ;
   yHidden  = false ;
   zHidden  = false ;
   pastX    = new short[pastDataSize];
   pastY    = new short[pastDataSize];
   pastZ    = new short[pastDataSize];
   currentData = 0;

 } 
 
 //----------------------------
 public void setDataSourceAsFile(){   dataSource =  FILE_AS_SOURCE ;  }
 
 //----------------------------
 public void saveAccelerometerData(){
   if (!dataFileHasAName) return;
   
   if (this.name.equals(accelerometers.get(accelerometers.size() -1).name)){
         
         for (int i=0; i< pastDataSize;i++) {
           for (short currentAccelero = 0; currentAccelero < accelerometers.size(); currentAccelero++){
             output.print(accelerometers.get(currentAccelero).pastX[i] +"," + accelerometers.get(currentAccelero).pastY[i] + "," + accelerometers.get(currentAccelero).pastZ[i] + ",   " );
           }
           output.println("");
         }
         recordingStopTime = millis();
       println("saving at " ,recordingStopTime);
   }
 }
 
 
 //----------------------------
 public void setActive(){
   isActive = true; 
 }
 
 //----------------------------
 public void setInactive(){
   isActive = false;
   x = 0;   y = 0;   z = 0;
 }
 
 public void setX(short newX){  x = newX; pastX[currentData] = x; }
 public void setY(short newY){  y = newY; pastY[currentData] = y; }   
 
 //----------------------------
 public void setZ(short newZ){  
   z = newZ; pastZ[currentData] = z;
   if (currentData < oldestDataPoint) currentData++;
   else {
     saveAccelerometerData();
     currentData = 0;
   }
 }   
 
 //------------------------------------------------------------------------------
 public void show(int baseScreenPosition){

    int j = currentData; //isolate show from changes in currentData while graphing
    strokeWeight(2);
    strokeJoin(ROUND);
    strokeCap(ROUND);
    
    for (int currentPixel = startOfChartXAxis ; currentPixel < screenSize-1; currentPixel++ ) {
      int firstDataPoint  =  j - (currentPixel*dataGraphResolution)                       ;
      int secondDataPoint =  j - (currentPixel*dataGraphResolution) - dataGraphResolution ;
      
      if (firstDataPoint  < 0) firstDataPoint  = oldestDataPoint + firstDataPoint  ;
      if (secondDataPoint < 0) secondDataPoint = oldestDataPoint + secondDataPoint ;
      

      if (!xHidden){
        if (isActive) { stroke(46 + (pastX[firstDataPoint]) , 209, 2+ (pastX[firstDataPoint] )) ; /*fill(46 + (pastX[firstDataPoint] >>YAxisScale) , 209, 2+ (pastX[firstDataPoint] >>YAxisScale)) ;*/}
        else          { stroke(200, 12, 15); fill(200, 12, 15); }
        line(currentPixel-1   ,  baseScreenPosition - (pastX[firstDataPoint] >>YAxisScale), 
             currentPixel ,  baseScreenPosition - (pastX[secondDataPoint]>>YAxisScale) );//curveVertex(10+ i, pastX[i]/2);
      }
      
      if (!yHidden){
        if (isActive) { stroke(0 + (pastX[firstDataPoint] ), 150, 250); /*fill(0, 150, 250);*/}
        else          { stroke(200, 12, 15); fill(200, 12, 15); }
        //point(startOfChartXAxis+ i, baseScreenPosition-(pastY[firstPoint]>>4));
        line(currentPixel-1   , baseScreenPosition - (pastY[firstDataPoint] >>YAxisScale) + graphAxisDisplayOffset,
             currentPixel , baseScreenPosition - (pastY[secondDataPoint]>>YAxisScale) + graphAxisDisplayOffset);//curveVertex(10+ i, pastX[i]/2);
      }
      
      if (!zHidden){
        if (isActive) { stroke(200, 2 + (pastX[firstDataPoint] ), 153);/*fill(200, 2, 153); */}
        else          { stroke(200, 12, 15); fill(200, 12, 15); }
        //point(startOfChartXAxis+ i, baseScreenPosition-(pastZ[firstPoint]>>4));
        line(currentPixel-1   , baseScreenPosition+(2*graphAxisDisplayOffset) - (pastZ[firstDataPoint]>>YAxisScale), 
             currentPixel , baseScreenPosition+(2*graphAxisDisplayOffset) - (pastZ[secondDataPoint]>>YAxisScale));//curveVertex(10+ i, pastX[i]/2);
      }
      
    }
    
 }
}



// -------------------------------------------------------
public void fileSelected(File selection) {
  if (selection == null)  {
    println("Window was closed or the user hit cancel.");
    exit(); 
  } 
  else{
  println("User selected " + selection.getAbsolutePath());
  reader = createReader(selection);
  }
}

//--------------------------------------------------------
public void readAccelerometerDataFromFile(){
  
  
  for(Accelerometer accel : accelerometers) accel.setDataSourceAsFile();
  
  
   try { line = reader.readLine(); } catch (IOException e) { e.printStackTrace(); line = null; }
   try { line = reader.readLine(); } catch (IOException e) { e.printStackTrace(); line = null; }
   if (line == null) return; 
   String[] data = split(line, ',');
 
   //writeToSensors(sensorData);
   accelerometers.get(0).setX( (short)PApplet.parseInt(data[0]));
   accelerometers.get(0).setY((short)PApplet.parseInt(data[1]));
   accelerometers.get(0).setZ((short)PApplet.parseInt(data[2]));
   
   accelerometers.get(1).setX((short)PApplet.parseInt(data[3]));
   accelerometers.get(1).setY((short)PApplet.parseInt(data[4]));
   accelerometers.get(1).setZ((short)PApplet.parseInt(data[5]));
   
   accelerometers.get(2).setX((short)PApplet.parseInt(data[6]));
   accelerometers.get(2).setY((short)PApplet.parseInt(data[7]));
   accelerometers.get(2).setZ((short)PApplet.parseInt(data[8]));
   
}

// ------------------------------------------------------
public void exit() {
  
  if (output != null){
    if (recordingStartTime > recordingStopTime) recordingStartTime = recordingStopTime; //fences empty recording sessions
    output.println("// Total Recording time is " + (recordingStopTime - recordingStartTime + " ms"));
    output.flush();
    output.close();
    
    serialPort.write('H'); // HALTS the arduino
    serialPort.stop();
  }
  super.exit();
}
  public void settings() {  size(1200,600,P3D); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "AcceleroDataCollector" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
