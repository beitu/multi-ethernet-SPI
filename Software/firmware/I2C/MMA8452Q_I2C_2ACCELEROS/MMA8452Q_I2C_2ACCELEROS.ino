#include <Wire.h>

/******************************************************************************
MMA8452Q_Basic.ino
SFE_MMA8452Q Library based of Jim Lindblom @ SparkFun Electronics
Davel Loper modified the library for making it multi-accelero friendly

Hardware hookup:
  Arduino --------------- MMA8452Q Breakout
    3.3V  ---------------     3.3V
    GND   ---------------     GND
  SDA (A4) --\/330 Ohm\/--    SDA
  SCL (A5) --\/330 Ohm\/--    SCL

The MMA8452Q is a 3.3V max sensor, so you'll need to do some 
level-shifting between the Arduino and the breakout. Series
resistors on the SDA and SCL lines should do the trick. Or 
better you can use a 3.3V arduino.

Distributed as-is; no warranty is given.
******************************************************************************/

//#include <SFE_MMA8452Q.h> // Includes the SFE_MMA8452Q library
#include"accelerometer_MMA8452Q.h"
#define BUFFER_SIZE 128
#define ACCELERO_DATA_SIZE 6
MMA8452Q accelerometer1;
MMA8452Q accelerometer2;
char acceleroData[BUFFER_SIZE];

// The setup function simply starts serial and initializes the
//  accelerometer.
void setup()
{
  
  Serial.begin(115200) ; // 230400);
    
  accelerometer1.init(FIRST_ACCELERO ,SCALE_8G,ODR_800);
  accelerometer2.init(SECOND_ACCELERO,SCALE_8G,ODR_800);
  while (Serial.available() == 0);
}

// The loop function will simply check for new data from the
//  accelerometer and print it out if it's available.
void loop()
{
  
  unsigned int currentBufferIndex = 0 ;
  unsigned int totalLoops = BUFFER_SIZE/ACCELERO_DATA_SIZE ;
  
  for (unsigned int bufferLoop = 0; bufferLoop < totalLoops ;bufferLoop++){
  
  if (accelerometer1.available()) accelerometer1.read(); 
  if (accelerometer2.available()) accelerometer2.read(); 

  
  acceleroData[currentBufferIndex++] = accelerometer1.x ;//& 0xFF; 
  acceleroData[currentBufferIndex++] = accelerometer1.y ;//& 0xFF; 
  acceleroData[currentBufferIndex++] = accelerometer1.z ;//& 0xFF; 
  
  acceleroData[currentBufferIndex++] = accelerometer2.x ; //& 0xFF; 
  acceleroData[currentBufferIndex++] = accelerometer2.y ;//& 0xFF; 
  acceleroData[currentBufferIndex++] = accelerometer2.z ;//& 0xFF; 
  

  delayMicroseconds(625);
  }
  

/* for (int i =0 ; i < 12; i++) {
    Serial.print(acceleroData[i],DEC);
    Serial.print('~');
  }
  for (int i =12 ; i < 24; i++) {
    Serial.print(acceleroData[i],DEC);
    Serial.print(' ');
  }
  Serial.println();*/
  
  for (int i =0 ; i < currentBufferIndex; i++)  { Serial.print(acceleroData[i]);
   // Serial.print((byte)acceleroData[i]);
    //Serial.print((byte)acceleroData[i]>>8);
  }
  Serial.flush();
    
    
}

// The function demonstrates how to use the accel.x, accel.y and
//  accel.z variables.
// Before using these variables you must call the accel.read()
//  function!
void printAccels(MMA8452Q currentAccelero)
{
 
    
  
}

// This function demonstrates how to use the accel.cx, accel.cy,
//  and accel.cz variables.
// Before using these variables you must call the accel.read()
//  function!
void printCalculatedAccels(MMA8452Q currentAccelero)
{ 
  Serial.print(currentAccelero.x, 3);
  Serial.print("\t");
  Serial.print(currentAccelero.y, 3);
  Serial.print("\t");
  Serial.print(currentAccelero.z, 3);
  Serial.print("\t");
}

