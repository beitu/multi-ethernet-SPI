#include "GenericSensor.h"

  int GenericSensor::refreshRate    = ONE_SECOND   ; // default value to be modified by Derived sensor class
  int GenericSensor::sensorDataSize = DEFAULT_SENSOR_DATA_SIZE ;

  void GenericSensor::setRefreshRate(int refresh){ refreshRate = refresh;  }

  void GenericSensor::setDataSize( int size){ sensorDataSize = size ;  }

