#include "Arduino.h"
#include <vector>

#ifndef GenericSensor_h
#define GenericSensor_h
#define ONE_SECOND 1000000
#define DEFAULT_SENSOR_DATA_SIZE 0

class GenericSensor{
public:

    // FUNCTIONS TO IMPLEMENT IN THE DERIVED CLASS
    // ------------------------------------
    virtual void initializeAt(int address);  // Initializes the SPI CS of a sensor
    virtual bool sensorIsConnected()    ;    // return connection status of a sensor 
    virtual void readSensor(byte* data) = 0; // read the data from the sensor, pass a pointer 
    // ------------------------------------

    
    void setRefreshRate(int refresh);
    void setDataSize( int size);

    //static functions can be accessed without using an instance of sensor object
    static int getRefreshRate()    { return GenericSensor::refreshRate;   };
    static int getSensorDataSize() { return GenericSensor::sensorDataSize; };    
    
    
private:
  bool sensorConnectionStatus = false;
  static int refreshRate    ; // initiatlize this to the refreshrate of the sensor (in the constructor of the derived class)   
  static int sensorDataSize ;   
};


#endif

