#include "Arduino.h"
#include <vector>


#ifndef SensorHub_h
#define SensorHub_h

#define MAX_SENSOR_COUNT 12
#define DEFAULT_SENSOR_COUNT 0
#define MAX_READING_BEFORE_SENDING 128
#define FIRST_SPI_PIN       2

//This is only used when serializing settings to send them over to the computer as a byte array
using SerializedHubSettings = struct {
  short sensorsPluggedIn    = DEFAULT_SENSOR_COUNT;
  short sensorReadingsInHub = MAX_READING_BEFORE_SENDING ;
  short bufferSize          = 0 ;
};


//----------------------------------------
// Arduino does not support STL Algorithm
// and even less sorting algorithm
//----------------------------------------
template <class A> void sort(A& container){
   for(int currentElement=0; currentElement< container.size(); currentElement++) {
        for(int currentSortingElement=0; currentSortingElement<(container.size()-(currentElement+1)); currentSortingElement++) {
                if(container[currentSortingElement] > container[currentSortingElement+1]) {
                    auto elementToSwap = container[currentSortingElement];
                    container[currentSortingElement] = container[currentSortingElement+1];
                    container[currentSortingElement+1] = elementToSwap;
                }
        }
    }
}

// ----------------------------------------------------------------------------------------------
template <class SENSOR_CLASS> class SensorHub {
  public:
  
    byte* sensorsData = nullptr ; 
    int   bufferSize  = 0       ;

    //----------------------------------------
    SensorHub() {   /*needs explicit constructor*/    }

    
    //----------------------------------------
    void updateSensorCount() {
      int currentPin {0} ;
      for ( auto& currentSensor : sensors){
          bool wasPluggedUntilNow = false ;
         
          for (auto& currentPluggedSensor : pluggedSensors) if (currentPluggedSensor == currentPin) wasPluggedUntilNow = true ; //was the sensor connected until now? for loop for constant time
          if (!wasPluggedUntilNow) currentSensor.initializeAt(FIRST_SPI_PIN + currentPin) ; //if not plugged yet, try to plug         
          if (currentSensor.sensorIsConnected()) if (!wasPluggedUntilNow) pluggedSensors.push_back(currentPin); //wasn't connectd, need to be there now
          else if (wasPluggedUntilNow) {
            //removes the pin as part of currently connected
            auto it = pluggedSensors.begin();
            while(*it != currentPin) it++; 
            pluggedSensors.erase(it);
          }          
          currentPin++;
      }
      sort(pluggedSensors);
      
     
      
      //create settings for the hub
      sensorReadingsInHub      =  MAX_READING_BEFORE_SENDING / pluggedSensors.size() ;
      bufferSize               = SENSOR_CLASS::getSensorDataSize() * sensorReadingsInHub * pluggedSensors.size() ;
      delayToNotOverflowSensor = SENSOR_CLASS::getRefreshRate() / pluggedSensors.size();

      if (sensorsData != nullptr ) free(sensorsData );
      sensorsData = new byte[bufferSize];
    }

    //----------------------------------------
    void readSensors() {

      unsigned int currentBufferIndex = 0 ;
      int sensorDataByteSize = SENSOR_CLASS::getSensorDataSize();
      
      for (int currentReading = 0 ; currentReading < sensorReadingsInHub; currentReading++) {        
        for (auto& currentPluggedSensor : pluggedSensors) {
          sensors[currentPluggedSensor].readSensor(sensorsData + currentBufferIndex);
          currentBufferIndex += sensorDataByteSize ;
          delayMicroseconds(delayToNotOverflowSensor); // taking care not to be too fast for the sensor
        };
      }
      
    }
   

    //----------------------------------------
    SerializedHubSettings getHubSettings() {
      SerializedHubSettings currentSettings;
      currentSettings.sensorsPluggedIn    = pluggedSensors.size() ;
      currentSettings.sensorReadingsInHub = sensorReadingsInHub ;
      currentSettings.bufferSize          = bufferSize ;
      return currentSettings ;
    }

    //----------------------------------------
    int getBufferByteSize(){ return bufferSize; }
     
  private:
    std::vector<int> pluggedSensors ;
    std::vector<SENSOR_CLASS> sensors{MAX_SENSOR_COUNT };
    int sensorReadingsInHub      = MAX_READING_BEFORE_SENDING ;
    int delayToNotOverflowSensor = 0 ;
};


#endif

