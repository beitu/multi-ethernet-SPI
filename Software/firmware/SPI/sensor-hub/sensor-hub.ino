//----------------------------------------------
//
// I M P O R T A N T  N O T E:
//
//  You need to follow instructions at :
// https://community.platformio.org/t/how-to-increase-the-arduino-SerialUSB-buffer-size/122
// 
// This will help you change the size of the SerialUSB buffer, which is needed for speed
//
//----------------------------------------------
#include "Adxl.h"
#include "SensorHub.h"

//Serial MAXIMUM SPEEDS
// FOR STANDARDS ARDUINO, 115200 or 250000
// FOR ARDUINO DUE STANDARD USB PORT (called programming port) 460800 
// FOAR ARDUINO DUE FAST USB PORT (called native port) 5000000 

#define Serial_SPEED  5000000 // 460800 //5000000 
#define NO_DATA_YET         0

//----------------------------------------------
SensorHub <Accelerometer> sensorHub ;


//----------------------------------------------
void sendSetupToComputer(){
  
  
  int communicationTimeout = 10 ;
  char startCharacter = ' ' ;
  
    
  //HALTS ARDUINO 
  while ( (SerialUSB.available() == NO_DATA_YET) ); 
  while (startCharacter != 'H') if (SerialUSB.available() != NO_DATA_YET) startCharacter = SerialUSB.read();

  
  //UNTIL computer FINISHED EXISTING DATA AND SEND 'R' for ready
  while (startCharacter != 'R') if  (SerialUSB.available() != NO_DATA_YET) startCharacter = SerialUSB.read();
  sensorHub.updateSensorCount();

  
  //sending settings to the computer
  SerializedHubSettings settings = sensorHub.getHubSettings();
  SerialUSB.write ( (byte*) &settings, Accelerometer::getSensorDataSize());
  SerialUSB.flush();

  
  while (startCharacter != 'S')  if (SerialUSB.available() != NO_DATA_YET ) startCharacter = SerialUSB.read();

  
  
 
}


//----------------------------------------------
void setup() {
  SerialUSB.begin(Serial_SPEED); while(!Serial);
  SerialUSB.setTimeout(1);
  
  SerialUSB.print("SPI DATA COLLECTOR\n\nSetting up SPI... "); SerialUSB.flush();
  SPI.begin();
  SPI.beginTransaction(SPISettings(ADXL_MAX_SPEED, MSBFIRST, SPI_MODE3)); 
  SerialUSB.println("OK");
  
  SerialUSB.print("Setting up sensors...");
  SerialUSB.flush();
  sensorHub.updateSensorCount();
  SerialUSB.println("OK");

  SerialUSB.println("Waiting for signal to start reading sensors...");
  SerialUSB.flush();

  sendSetupToComputer(); 
}


//----------------------------------------------
void loop() {
  
  //NOTE: the line below can't be replaced by a SerialEvent since it's incompatible with Arduino Due native port
  sensorHub.readSensors();   
  if (SerialUSB.available() != NO_DATA_YET )  sendSetupToComputer();  
  SerialUSB.write(sensorHub.sensorsData, sensorHub.bufferSize );
  SerialUSB.flush();
}



//----------------------------------------------
//----------------------------------------------
