//----------------------------------------------
#include <Adxl345.h>

//
// This test code was made for the Arduino Due, 
// The default SPI speed is 4Mhz

#define SERIAL_SPEED 250000
#define SPI_PIN 4
//----------------------------------------------

int x,y,z                   ;
Accelerometer test(SPI_PIN) ;


//----------------------------------------------
void setup() {
  Serial.begin(SERIAL_SPEED);
  test.powerOn();
}

//----------------------------------------------
void loop() {

  test.readAccel(&x, &y, &z);
  Serial.print(x);
  Serial.print(", ");
  Serial.print(y);
  Serial.print(", ");
  Serial.println(z);
  //delay(100);
}

//----------------------------------------------
//----------------------------------------------
